## Changelog

This document will list all new items added to ids.csv, as well as other notes.
Items will be listed in the form

>NAME (ID)

#### March 1, 2017
 * Gem sack token (39686)

#### February 22, 2017
 * Death notes (39573)
 * Nex: Angel of Death items:
   * Wand of the praesul (39574)
   * Wand of the praesul shard (39576)
   * Imperium core (39579)
   * Imperium core shard (39581)
   * Praesul codex (39584)
   * Intricate blood stained chest (39586)
   * Intricate ice chest (39588)
   * Intricate shadow chest (39590)
   * Intricate smoke-shrouded chest (39592)

#### January 5, 2017
Net was out, sorry for huge delay.
 * Silver jewelry (December 12, 2016)
   * Opal ring (39322)
   * Opal bracelet (39324)
   * Opal necklace (39326)
   * Opal amulet (39328)
   * Opal amulet (39330) (unstrung)
   * Jade ring (39332)
   * Jade bracelet (39334)
   * Jade necklace (39336)
   * Jade amulet (39338)
   * Jade amulet (39340) (unstrung)
   * Topaz ring (39342)
   * Topaz bracelet (39344)
   * Topaz necklace (39346)
   * Topaz amulet (39348)
   * Topaz amulet (39350) (unstrung)
   * Ring of metamorphosis (39352)
   * Headhunter's sacrifice (39355)
   * Featherfingered necklace (39358)
   * Amulet of bountiful harvest (39361)
   * Ring of respawn (39364)
   * Flamtaer bracelet (39367)
   * Traveller's necklace (39370)
   * Botanist's amulet (39373)
   * Columbarium ring (39376)
   * Headhunter's thrill (39379)
   * Necklace of gluttony (39382)
   * Enlightened amulet (39385)
 * New Year's resolutions TH promotion
   * Conga dance override token (39423)

#### December 5, 2016
 * Missed a week or two, oops.
 * Nightmare gauntlets (39248)
 * Woolly pudding hat (39269)
 * Terrorbird plushie (39271)
 * Christmas pudding balloon (39273)
 * Christmas tree kite (39275)

#### November 14, 2016
 * Bounty Hunter Redux
   * Ancient warriors' equipment patch (39047)
   * Annihilation (39049)
   * Decimation (39053)
   * Obliteration (39057)
   * Amulet of the forsaken (39061)
   * Wilderness hilt (39065)
   * Adrenaline crystal (39067)
   * Rainbow amulet token (30157)
   * Rainbow cape (39160)

#### October 31, 2016
 * Patch Week (new TH promo + urn)
   * Shard of chaos weapon token (38990)
   * Shard of despite weapon token (38991)
   * Shard of suffering weapon token (38992)
   * Mimic plushie (38993)
   * Decorated woodcutting urn (nr) (39010)

#### October 28, 2016
 * Halloween TH promotion:
   * Zombie walk override token (38964)

#### October 18, 2016
 * Removed two "Do not translate" entries, as they were removed from the GE.
 * No new IDs.

#### October 11, 2016
 * IDs added for The Arc - Chapter 2
   * Ikuchi orokami mask (38788)
   * Kodama orokami mask (38790)
   * Akkorokamui orokami mask (38792)
   * Karasu orokami mask (38794)
   * Akateko orokami mask (38796)
   * Nue orokami mask (38798)
   * Shinigami orokami mask (38800)
   * Oni orokami mask (38802)
   * Memory-crushing scrimshaw (38823)
   * Energy-gathering scrimshaw (38828)
   * Idol crabletine token (38833)
   * Do not translate - never seen. (38865) (really? another?)
   * Crystal tool siphon (38872)

#### October 4, 2016
 * "Do not translate-never seen" (38515) added, most likely an error. (again?)

#### September 27, 2016
 * IDs added for Deathmatch update
   * Paddewwa teleport (38463)
   * Senntisten teleport (38464)
   * Kharyll teleport (38465)
   * Lassar teleport (38466)
   * Dareeyak teleport (38467)
   * Carrallanger teleport (38468)
   * Annakarl teleport (38469)
   * Ghorrock teleport (38470)

#### September 23, 2016
 * ShellCheck script fixes.

#### September 22, 2016
 * IDs added
   * Solar Flare (38135)
   * Dagannoth Kings teleport (38203)
