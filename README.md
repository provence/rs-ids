RuneScape IDs
=============

A list of all tradable items, visible on the Grand Exchange, along with a helper script to fetch new ones.

Requirements
============
Python. Tested with 3.6, may work with lower. No third-party libraries are needed.

Usage
=====
```bash
$ python3 getids.py [END ID]
```

The end ID can be gotten from the [GE API](http://runeapps.org/apps/ge/browse.php?page=81). You may have to scroll a few pages ahead.