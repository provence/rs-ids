#!/bin/python

if __name__ != "__main__":
	exit()

import time
import json
import signal
import argparse
from sys import exit
from os import path, getenv
from urllib.request import urlopen

start_id = None
end_id = None
url = "http://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item={}"
ids = open("ids.csv", "a+")

if not path.exists("last.id"):
	start_id = 0
else:
	with open("last.id", "rt") as f:
		start_id = int(f.read())

parser = argparse.ArgumentParser()
parser.add_argument("end")
parser.add_argument("-f", "--force-sleep", action="store_true", default=False)
args = parser.parse_args()
end_id = int(args.end)
current_id = start_id

should_sleep = (end_id - start_id) > 100 or args.force_sleep
times = 0
new = 0

def sleep():
	if not args.force_sleep:
		global times
		if times > 100:
			time.sleep(5)
		else:
			times += 1
	else:
		time.sleep(5)

def get_id(id):
	try:
		res = urlopen(url.format(id)).read().decode("utf-8")
		return json.loads(res)
	except:
		return None

def sigint_handler(signal, frame):
	ids.close()
	with open("last.id", "wt+") as f:
		f.write(str(current_id))
	exit(0)

signal.signal(signal.SIGINT, sigint_handler)

print("Starting at ID:", start_id)
print("Expecting", end_id - start_id, "iterations.")

if should_sleep:
	if args.force_sleep:
		print("Sleeping enabled after every iteration")
	else:
		print("Sleeping will be enabled after 100 iterations")

for id in range(start_id + 1, end_id + 1):
	current_id = id
	if should_sleep:
		sleep()
	item = get_id(id)
	if item:
		new += 1
		name = item['item']['name']
		s = "{},{}\n".format(name, id)
		ids.write(s)
		print(s)

ids.close()

with open("last.id", "wt+") as f:
	f.write(str(end_id))

print("Finished. Total:", new, "new IDs")